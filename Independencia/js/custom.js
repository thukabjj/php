$(document).ready(function(){

        $('.conteudo-crianca').hide();

        window.sr = ScrollReveal();
        sr.reveal('.navbar-nav .nav-link', { duration: 1500, mobile: false, distance: '20px'}, 200);
        sr.reveal('.sub-menu-opt', { duration: 1500, mobile: false }, 200);
        sr.reveal('#portfolio', { duration: 1500 , origin: 'top', reset: true}, 200);
        sr.reveal('#imunu', { duration: 1500 , reset: true}, 200);
        sr.reveal('section#historias', { duration: 1500, origin: 'top', distance: '50px', reset: true }, 200);
        sr.reveal('.conteudo-historia p', { duration: 1500, mobile: false});
        sr.reveal('.pergunta-historia', { duration: 1500, mobile: false});
        sr.reveal('.logo-borboleta', { duration: 3000,  distance: '0px'});
        sr.reveal('.logo', { duration: 2500, delay: 2000,  distance: '0px'});
        
        //scrollReveal.prototype.isElemInViewport = function () { return true; }        
                    
        /*Submenu topicos principais*/
        $('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    $('#sistemaimune').click(function(){

    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
 		$('.conteudo-sisimu').show();
        $('.conteudo-imunoprimary').show();
        $('.conteudo-tipoidp').hide();
 		$('.conteudo-sinidp').hide();

 		$('#btn-orgao').click(function(){

    	$('.conteudo-orgao').show();
        $('.btn-orgao').css('background-color','#5e8eca');
        $('.btn-orgao').css('border-color','#5e8eca');
    	$('.conteudo-sangue').hide();

    });

    $('#btn-sangue').click(function(){

    	$('.conteudo-sangue').show();
    	$('.conteudo-orgao').hide();

    });

    });
    $('#tipoidp').click(function(){

 		$('.conteudo-tipoidp').show();
 		$('.conteudo-sisimu').hide();
 		$('.conteudo-sinidp').hide();
        $('.conteudo-imunoprimary').hide();
        
    });
    $('#sintomaidp').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
 		$('.conteudo-sinidp').show();
 		$('.conteudo-sisimu').hide();
 		$('.conteudo-tipoidp').hide();
        $('.conteudo-imunoprimary').hide();
        
    });
    $('#terapiaimuno').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    	
 		$('.conteudo-terapiaimuno').show();
 		$('.conteudo-antibiotico').hide();
 		$('.conteudo-terapiacelulas').hide();
 		$('.conteudo-terapiagenes').hide();
 		$('.conteudo-saudavel').hide();
        $('.conteudo-imunoprimary').show();
    });
    $('#antibioticos').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    	
 		$('.conteudo-terapiaimuno').hide();
 		$('.conteudo-antibiotico').show();
 		$('.conteudo-terapiacelulas').hide();
 		$('.conteudo-terapiagenes').hide();
 		$('.conteudo-saudavel').hide();
    });
    $('#terapiacelulas').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    	
 		$('.conteudo-terapiaimuno').hide();
 		$('.conteudo-antibiotico').hide();
 		$('.conteudo-terapiacelulas').show();
 		$('.conteudo-terapiagenes').hide();
 		$('.conteudo-saudavel').hide();
    });
    $('#terapiagenes').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    	
 		$('.conteudo-terapiaimuno').hide();
 		$('.conteudo-antibiotico').hide();
 		$('.conteudo-terapiacelulas').hide();
 		$('.conteudo-terapiagenes').show();
 		$('.conteudo-saudavel').hide();
    });
    $('#saudavel').click(function(){
    	$('.imunodeficiencia-primaria .row').css('margin-bottom','0');
    	
 		$('.conteudo-terapiaimuno').hide();
 		$('.conteudo-antibiotico').hide();
 		$('.conteudo-terapiacelulas').hide();
 		$('.conteudo-terapiagenes').hide();
 		$('.conteudo-saudavel').show();
    });
    /*Submenu sub-topicos principais*/
   
    $('#btn-adulto').click(function(){

    	$('.conteudo-adulto').show();
    	$('.conteudo-crianca').hide();

    });
    $('#btn-crianca').click(function(){

    	$('.conteudo-crianca').show();
    	$('.conteudo-adulto').hide();

    });

      $("a.nav-link-imuno").click(function(){
        console.log($("a.nav-link-imuno").find("active").prevObject.removeClass("active"));
        $(this).addClass('active');
      });

      $("a.nav-link-opt").click(function(){
        console.log($("a.nav-link-opt").find("active").prevObject.removeClass("active"));
        $(this).addClass('active');
      });

      $("a.nav-link-terapia").click(function(){
        console.log($("a.nav-link-terapia").find("active").prevObject.removeClass("active"));
        $(this).addClass('active');
      });

      $('#btn-orgao').click(function(){
        $('.conteudo-orgao').show();
        $('.btn-orgao').css('background-color','#5e8eca');
        $('.btn-orgao').css('border-color','#5e8eca');
        $('.conteudo-sangue').hide();
    });

    $('#btn-sangue').click(function(){
        $('.conteudo-sangue').show();
        $('.conteudo-orgao').hide();
    });

      
});


