<?php 
class Usuario{

	private $id_usuario;
	private $deslogin;
	private $dessenha;
	private $dtcadastro;


	public function getIdUsuario(){
		return $this->id_usuario;
	}
	public function setIdUsuario($idusuario){
		$this->id_usuario = $idusuario;
	}


	public function getDeslogin(){
		return $this->deslogin;
	}
	public function setDeslogin($deslogin){
		$this->deslogin = $deslogin;
	}


	public function getDessenha(){
		return $this->dessenha;
	}
	public function setDessenha($dessenha){
		$this->dessenha = $dessenha;
	}


	public function getDtCadastro(){
		return $this->dtcadastro;
	}
	public function setDtCadastro($dtcadastro){
		$this->dtcadastro = $dtcadastro;
	}

	public function loadById($id){
		$sql = new Sql();
		$result = $sql->setSelect("SELECT * FROM tbl_usuario WHERE id_usuario = :ID", array(
			":ID"=>$id));

		if (count($result) > 0 ) {

			$this->setData($result[0]);

		}

	}


	public function __construct($login = "", $password = "")
	{
		$this->deslogin = $login;
		$this->dessenha = $password;
	}
	public function __toString(){
		return json_encode(array(
			'id_usuario'=>$this->getIdUsuario(),
			'deslogin'=>$this->getDeslogin(),
			'dessenha'=>$this->getDessenha(),
			'data_hora'=>$this->getDtCadastro()->format("d/m/Y H:i:s")
		));
	}


	public static function getList(){
		$sql = new Sql();
		return $sql->setSelect("SELECT * FROM tbl_usuario ORDER BY deslogin");
	}


	public static function Search($login){
		$sql = new Sql();
		return $sql->setSelect("SELECT * FROM tbl_usuario WHERE deslogin LIKE :SEARCH ORDER BY deslogin",array(
			':SEARCH'=>"%".$login."%"
			));
	}


	public function Login($login,$password){
		$sql = new Sql();

		$result = $sql->setSelect("SELECT * FROM tbl_usuario WHERE deslogin = :LOGIN AND dessenha = :PASSWORD", array(
			":LOGIN"=>$login,
			":PASSWORD"=>$password
			));

		if (count($result) > 0 ) {

			$this->setData($result[0]);

		} else{

			throw new Exception("Login/Senha Inválidos!");
			
		}
	}

	public function setData($data){
		$this->setIdUsuario($data['id_usuario']);
		$this->setDeslogin($data['deslogin']);
		$this->setDessenha($data['dessenha']);
		$this->setDtCadastro(new DateTime($data['data_hora']));
	}

	public function Insert(){
		$sql = new Sql();

		$result = $sql->setSelect("CALL sp_usuarios_insert(:LOGIN , :PASSWORD)", array(
			':LOGIN' =>$this->getDeslogin(),
			':PASSWORD'=>$this->getDessenha()
		));

		if (count($result)>0) {
			$this->setData($result[0]);
		}else{
			throw new Exception("Não tem Resultado");
			
		}
	}
	public function Update($login,$password){
		$this->setDeslogin($login);
		$this->setDessenha($password);


		$sql = new Sql();
		$sql->setQuery("UPDATE tbl_usuario SET deslogin = :LOGIN, dessenha = :PASSWORD WHERE id_usuario = :ID",array(
			':LOGIN'=>$this->getDeslogin(),
			':PASSWORD'=>$this->getDessenha(),
			':ID'=>$this->getIdUsuario()

			));
	}

	public function Delete(){
		$sql = new Sql();

		$sql->setQuery("DELETE FROM tbl_usuario WHERE id_usuario = :ID",array(
			':ID'=>$this->getIdUsuario()
			));
		$this->setIdUsuario(0);
		$this->setDeslogin("");
		$this->setDessenha("");
		$this->setdtcadastro(new DateTime());
	}
}


?>