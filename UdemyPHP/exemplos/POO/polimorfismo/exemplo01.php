<?php

abstract class Animal{


	public function falar(){
		return "Som";
	}
	public function mover(){


		return "Andar";
	}
}

class Cachorro extends Animal{

	public function falar(){

		return "Late";
	}

}

class Gato extends Animal{

	public function falar(){
		return "Mia";
	}

}
class Passaro extends Animal{
	public function falar(){

		return "Cantar";

	}
	public function  mover(){

		return "Voar e ". parent::mover();

	}
}

$piriquito = new Passaro();
$garfield = new Gato();
$pluto = new Cachorro();

echo $pluto->falar()."<br>";
echo $pluto->mover()."<br>";
echo "<hr>";
echo $garfield->falar()."<br>";
echo $garfield->mover()."<br>";
echo " <hr>";
echo $piriquito->falar()."<br>";
echo $piriquito->mover()."<br>";


?>