<?php
$hierarquia2 = array(
	array(
		//Inicio:  CEO
		'nome_cargo'=>'CEO',
		'subordinados'=>array(
			//Inicio: Diretor Comercial
			array(
				'nome_cargo'=>'Diretor Comercial',
				'subordinados'=> array(
					//Inicio: Gerente de Vendas
					array(
						'nome_cargo'=>'Gerente de Vendas'
					)//Termino: Gerente de Vendas
				) 
			),
			//Terimino: Diretor Cormecial
			//Inicio:  Diretor Funanceiro
			array(
				'nome_cargo'=>'Diretor Financeiro',
				'subordinados'=>array(
					//Inicio:  Gerente de Contas a Pagar
					array(
						'nome_cargo'=>'Gerente de Contas a Pagar',
						'subordinados'=>array(
							//Inicio:  Suporvisor de Pagamentos
							array(
								'nome_cargo'=>'Suporvisor de Pagamentos'
							)//Termino:  Suporvisor de Pagamentos
						)
					),//Termino:  Gerente de Contas a Pagar
					//Inicio:  Gerente de Compras
					array(
						'nome_cargo'=>'Gerente de Compras',
						'subordinados'=>array(
							//Inicio:  Supervisor de Suprimentos
							array(
								'nome_cargo'=>'Supervisor de Suprimentos'
							)//Termino:  Supervisor de Suprimentos
						)
					)//Termino:  Gerente de Compras
				)
			)//Termino:  Diretor Funanceiro
		)
	)//Termino:  CEO
);

function exibe($cargos){


	$html  = '<ul>';


	foreach ($cargos as $cargo) {

		$html .= "<li>";

		$html .= $cargo['nome_cargo'];

		if (isset($cargo['subordinados']) && count($cargo['subordinados']) > 0) {
			$html .= exibe($cargo['subordinados']);
		}
		$html .="</li>";
	}

	$html .= "</ul>";

	return $html;
}
echo exibe($hierarquia2);
?>