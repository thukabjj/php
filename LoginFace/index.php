<!DOCTYPE html>
<html>
<head>
	<title>Exemplo de Login com o Facebook em JavaScript</title>
	<meta charset="utf-8">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
</head>
<body>

<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>

	<p id="info">Olá Visitante!</p>
		<p id="foto"></p>

	<div id="status">
	</div>











	<script>
	// Isso é chamado com os resultados da função FB.getLoginStatus().
	function statusChangeCallback(response){
		console.log('statusChangeCallback');
		console.log(response);
			//O objeto de resposta é retornado com um campo de status que permite o app saber qual estado de autenticação do usuario.
			if (response.status === 'connected') {
				//Autenticado com o seu app e com o Facebook
				//Executa a função usuarioConectado().
				usuarioConectado();
			} else if (response.status === 'not_authorized') {
				// O usuario esta logado no facebook, mas nao no seu app
				document.getElementById('status').innerHTML = 'Favor, faça seu  login'+'neste app.';
			}else{
				// O usuario nao esta logado no Facebook, portanto não da para definir se esta logado no app ou não
			}
		}
			function checkLoginState() {
				FB.getLoginStatus(function(response){
					statusChangeCallback(response);
				});
			}
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '1592453227488915',
					cookie     : true,	//habilita os cookies para que o seu servidor acesse a sessão remotamente
					xfbml      : true, 	//permite marcações sociais nesta pagina
					version    : 'v2.1' //use a versão 2.1
				});
				// Agora que a SDK JavaScript foi inicializada, vamos chamar a função FB.getLoginStatus(); Essa função
				// verifica o estado do usuario quando ele visita esta pagina e pode indicar 3 estados possiveis na resposta. Eles podem ser:
				// 1. Autenticado no seu app('connected')
				// 2. Autenticado no Facebook, mas não no seu app ('not_authorized')
				// 3. Não esta autenticado no FaceBook, nem no seu app
				FB.getLoginStatus(function(response){
					statusChangeCallback(response);
				});

			};

			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			// Função de exemplo que é executada quando o usuario esta logado e ja deu as permissoes para o aplicativo
			function usuarioConectado() {
				//Com a função FB.api, é possivel fazer chamadas para o Graph API
				//Usando o parametro /me , você pode solicitar informações do Usuario conectado.
				FB.api('/me', function(response){
					$('#info').html(
						'Nome: ' + response.name +
						'<br>username: ' +response.username +
						'<br>Link perfil: ' +response.link +
						'<br>Email: ' +response.email +
						'<br>Sexo: ' +response.gender +
						'<br>ID: ' +response.id 
					);
					//Um exemplo de como foce pode recuperar a foto do usuario.
					$('#foto').html('<img src = "https://graph.facebook.com/'+response.username+'/picture" alt="'+response.name+'"/>')
				});
			}
	</script>
	
	
</body>
</html>
