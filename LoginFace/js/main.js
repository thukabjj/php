﻿$(function(){
	 $('input[maxlength]').maxlength({
            alwaysShow: true,
            threshold: 10,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger"
        });
	
	var $doc = $('html, body');
	$('.scrollSuave').click(function() {
    	$doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 1000);
    return false;
	});

});