var timeId = null;//variavel que armarzena a chamada da função timeout
function iniciaJogo(){


	var url = window.location.search;

	var nivel_jogo = url.replace('?',"");

	var tempo_segundos = 0;


	//validando nivel e atribuindo segundos conforme o nivel

	if (nivel_jogo == 1) {
		tempo_segundos = 120;
	}
	if (nivel_jogo == 2) {
		tempo_segundos = 60;
	}
	if (nivel_jogo == 3) {
		tempo_segundos = 30;
	}
	//inserindo segundos no span
	document.getElementById('cronometro').innerHTML = tempo_segundos;

	//quantidades de baloes 
	var qtde_baloes = 30;

	cria_baloes(qtde_baloes);
	//imprimindo a quantidade de balões inteiros
	document.getElementById('baloes_inteiros').innerHTML = qtde_baloes;
	document.getElementById('baloes_estourados').innerHTML = 0;
	contagem_tempo(tempo_segundos +1);
}
function game_over(){
	alert('Fim de Jogo, você não conseguiu estourar todos os balões a tempo.');
	remove_eventos_baloes();
}
function contagem_tempo(segundos){
	segundos--;

	if (segundos == -1) {
		clearTimeout(timeId);//para a execução da função do setTimeOut
		game_over();
		return false;
	}
	document.getElementById('cronometro').innerHTML = segundos;

	timeId = setTimeout("contagem_tempo("+segundos+")",1000);

}
function cria_baloes(qtde_baloes){

	for (var i =1; i <= qtde_baloes; i++) {

		var balao = document.createElement('img');
		balao.src = 'imagens/balao_azul_pequeno.png';
		balao.style.margin = '10px';
		balao.id='b' + i;
		balao.onclick = function(){
			estourar(this);
		}
		document.getElementById('cenario').appendChild(balao);
	}
}
function estourar(balao){

	var id_balao = balao.id;


	document.getElementById(id_balao).setAttribute("onclick","");
	document.getElementById(id_balao).src = "imagens/balao_azul_pequeno_estourado.png";
	pontuacao(-1);
	//alert(id_balao);
}
function pontuacao(acao){

	var baloes_inteiros = document.getElementById("baloes_inteiros").innerHTML;
	var baloes_estourados = document.getElementById("baloes_estourados").innerHTML;

	baloes_inteiros = parseInt(baloes_inteiros);
	baloes_estourados = parseInt(baloes_estourados);


	baloes_inteiros = baloes_inteiros + acao; 
	baloes_estourados = baloes_estourados - acao;

	document.getElementById('baloes_inteiros').innerHTML = baloes_inteiros;
	document.getElementById('baloes_estourados').innerHTML = baloes_estourados;

	situacao_jogo(baloes_inteiros);
} 
function situacao_jogo(baloes_inteiros){
	if (baloes_inteiros == 0) {
		
		alert('Parabéns, você conseguiu estourar todos os baloes a tempo.');
		para_jogo();
	}
}
function remove_eventos_baloes(){
	var i = 1; // contador para recuperar os baloes por id
	while(document.getElementById('b'+1)){
		//retirando o evento onclick do elemento
		document.getElementById('b'+i).onclick = '';
		i++;
	}

}
function para_jogo(){
	clearTimeout(timeId);
}