<?php
class Sql extends PDO {

	private $conn;

	public function __construct(){

		$this->conn = new PDO("mysql:host=localhost;dbname=dbphp7","root","");
	}
	private function setParams($stmt, $parameters = array()){
		foreach ($parameters as $key => $value) {

			$this->setParam($stmt,$key,$value);

		}
	}
	
	private function setParam($stmt,$key,$value){
		$stmt->bindParam($key, $value);
	}

	public function setQuery($rawQuery, $params = array()){
		$stmt = $this->conn->prepare($rawQuery);
		$this->setParams($stmt,$params);
	    $stmt->execute();
	    return $stmt;
	}
	public function setSelect($rawQuery,$params = array()):array
	{
		$stmt = $this->setQuery($rawQuery,$params);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}

?>