<?php 
$hierarquia = array(
	array(
		'nome_tema'=>'Disperdicio na Colhedora',
		'subtema'=>array(
			array(
				'nome_tema'=>'Condutores hidraulicos(A1)',
				'subtema'=>array(
					array(
						'nome_tema'=>'Mangueiras(A2)',
						'subtema'=>array(
							array(
								'nome_tema'=>'Ressecamento(A3)'
							),
							array(
								'nome_tema'=>'Abrasão(A3)'
							)
						)
					),
					array(
						'nome_tema'=>'Conexão(A2)',
						'subtema'=>array(
							array(
								'nome_tema'=>'Desaperto(A3)'
							),
							array(
								'nome_tema'=>'Trincas(A3)'
							)
						)
					)
				)
			),
			array(
				'nome_tema'=>'Motores hidraulicos(B1)',
				'subtema'=>array(
					array(
						'nome_tema'=>'Anéis e Vedações(B2)',
						'subtema'=>array(
							array(
								'nome_tema'=>'Ressecamento(B3)'
							),
							array(
								'nome_tema'=>'Rompimento(B3)'
							)
						)
					),
					array(
						'nome_tema'=>'Desapertos(B2)',
						'subtema' =>array(
							array(
								'nome_tema'=>'Fixação(B3)'
							),
							array(
								'nome_tema'=>'Quebra Fixação(B3)'
							)
						)
					)
				)
			),
			array(
				'nome_tema'=>'Pistões(C1)',
				'subtema'=>array(
					array(
						'nome_tema'=>'Trincas(C2)',
						'subtema'=>array(
							array(
								'nome_tema'=>'Fadiga(C3)'
							),
							array(
								'nome_tema'=>'Quebra Acidentaç(C3)'
							)
						)
					),	
					array(
						'nome_tema'=>'Anéis e Vedações(C2)',
						'subtema'=>array(
							array(
								'nome_tema'=>'Ressecamento(C3)'
							),
							array(
								'nome_tema'=>'Rompimento(C3)'
							)
						)
					)
				)
			)
		)
	)	
);

function renderizar($temas){
	$html  = '<ul>';
		foreach ($temas as $tema) {
			$html .= '<li>';
				$html .= $tema['nome_tema'];
				if (isset($tema['subtema']) && count($tema['subtema']) > 0) {
					$html .= renderizar($tema['subtema']);
				}
			$html .= '</li>';
		}
	$html .= '</ul>';
return $html;
}
echo json_encode($hierarquia);
?>